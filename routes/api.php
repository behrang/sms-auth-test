<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api\V1', 'prefix' => 'v1.0'], function () {
    
    Route::post('/auth/signup', 'AuthController@signup')->name('api.auth.signup');
    Route::post('/auth/signin', 'AuthController@signin')->name('api.auth.signin');
    Route::post('/auth/recovery', 'AuthController@recovery')->name('api.auth.recovery');
    
});