<?php

namespace Tests;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Support\Facades\Artisan;

abstract class ApiTestCase extends TestCase
{
    use DatabaseMigrations, WithFaker;
    
    protected $stack;
    
    protected $refreshDB = true;
    
    public function setUp()
    {
        parent::setUp();
        if ($this->refreshDB) {
            $this->artisan('migrate:fresh');
        } else {
            $this->artisan('migrate');
        }

        $this->stack = [];
    }
    
    /**
     * Create new user
     * 
     * @todo It is better to use database factory for creatting user
     * 
     * @return User
     */
    protected function createNewApiUser()
    {
        $phone = $this->faker->unique()->e164PhoneNumber;
        $user = User::create( [
            'name'          => $this->faker->name,
            'email'         => $this->faker->email,
            'password'      => Hash::make($phone),
            'phone_number'  => $phone
        ]);
        
        return $user;
    }
    
    public function successSendToken($phoneNumber)
    {
        $token = $this->faker->randomNumber(4);
        $this->stack[$phoneNumber] = $token; 
        return $token;
    }
    
    public function failedSendToken($phoneNumber)
    {
        return false;
    }
    
    public function verifyToken($phoneNumber, $token)
    {
        if (array_key_exists($phoneNumber, $this->stack) &&
            $this->stack[$phoneNumber] == $token) {
                return true;
        }
        return false;
        
    }
    
    /* public function successVerifyToken()
    {
        $token = $this->faker->randomNumber(4);
        $this->token = $token;
        array_push($this->stack, $token);
        $this->assertEquals($token, array_pop($this->stack));
        $this->assertTrue(empty($this->stack));
    }
    
    public function failedVerifyToken()
    {
        $token = $this->faker->randomNumber(4);
        $this->token = $token;
        array_push($this->stack, $token);
        $this->assertEquals($token . $this->faker->randomNumber(1), array_pop($this->stack));
        $this->assertTrue(empty($this->stack));
    } */
    
    /**
     * Prevent reset database
     */
    public function tearDown()
    {
        if ($this->refreshDB) {
            $this->artisan('migrate:reset');
            parent::tearDown();
        }
    }
    
    /**
     * Boot the testing helper traits.
     *
     * @return array
     */
    protected function setUpTraits()
    {
        if ($this->refreshDB) {
            return parent::setUpTraits();
        } else {
            $uses = array_flip(class_uses_recursive(static::class));
            
            unset($uses[RefreshDatabase::class]);
            unset($uses[DatabaseMigrations::class]);
            
            if (isset($uses[RefreshDatabase::class])) {
                $this->refreshDatabase();
            }
            
            if (isset($uses[DatabaseMigrations::class])) {
                $this->runDatabaseMigrations();
            }
            
            if (isset($uses[DatabaseTransactions::class])) {
                $this->beginDatabaseTransaction();
            }
            
            if (isset($uses[WithoutMiddleware::class])) {
                $this->disableMiddlewareForAllTests();
            }
            
            if (isset($uses[WithoutEvents::class])) {
                $this->disableEventsForAllTests();
            }
            
            if (isset($uses[WithFaker::class])) {
                $this->setUpFaker();
            }
            
            return $uses;
        }
        
    }
}
