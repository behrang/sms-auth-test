<?php

namespace Tests\Feature;

use Tests\ApiTestCase;

class ApiLoginTest extends ApiTestCase
{
    protected $refreshDB = false;
    
    /**
     * Login with new phone number
     *
     * @return void
     */
    public function testSuccessLoginWithNotExistingUser()
    {
        // Signup with new user
        $phoneNumber = $this->faker->unique()->e164PhoneNumber;
        $signupResponse = $this->json(
            'post',
            route('api.auth.signup'),
            ['phone_number' => $phoneNumber]
        );
        $signupResponse
            ->assertStatus(200)
            ->assertJson(['status' => 'success'])
        ;
        
        // Get sms code and checking it has only 4 digit
        $smsToken = $this->successSendToken($phoneNumber);
        $this->assertTrue(is_int($smsToken));
        $this->assertTrue((1000 <= $smsToken) && ($smsToken <= 9999));
        
        // Verifying sms code
        $tokenVerified = $this->verifyToken($phoneNumber, $smsToken);
        $this->assertTrue($tokenVerified);
        
        // Login with phone number and sms token
        $signinResponse = $this->json(
            'post',
            route('api.auth.signin'), [
                'phone_number' => $phoneNumber,
                'verification_code' => $smsToken,
            ]
        );
        $signinResponse
            ->assertStatus(200)
            ->assertJsonStructure([
                'jsend',
                'status',
                'data' => ['token'],
            ])
            ->assertJson(['status' => 'success'])
        ;
    }
    
    /**
     * Login with existing phone number
     * 
     *
     * @return void
     */
    public function testSuccessLoginWithExistingUser()
    {
        // Create user and signup with it
        $user = $this->createNewApiUser();
        $signupResponse = $this->json(
            'post',
            route('api.auth.signup'),
            ['phone_number' => $user->phone_number]
        );
        $signupResponse
            ->assertStatus(200)
            ->assertJson(['status' => 'success'])
        ;
        
        // Signin with user to create token for it
        $signinResponse = $this->json(
            'post',
            route('api.auth.signin'), [
                'phone_number' => $user->phone_number,
                'verification_code' => $this->successSendToken($user->phone_number),
            ]
        );
        $signinResponse
            ->assertStatus(200)
            ->assertJsonStructure([
                'jsend',
                'status',
                'data' => ['token'],
            ])
            ->assertJson(['status' => 'success'])
        ;
        
        // Recover forgotten token with phone number
        $signinResponse = $this->json(
            'post',
            route('api.auth.recovery'), [
                'phone_number' => $user->phone_number,
            ]
        );
        $signinResponse
            ->assertStatus(200)
            ->assertJsonStructure([
                'jsend',
                'status',
                'data' => ['token'],
            ])
            ->assertJson(['status' => 'success'])
        ;
    }
}
