# Laravel SMS Auth Testing Project

Creatting an API using the Laravel PHP framework to model SMS authentication service.

### Schema for apitokens table:
- id
- user_id
- token
- deleted_at
- created_at
- updated_at

### Schema for users table:
Using standard Laravel user table schema with add one column: 
- phone_number

## Installation

- Copy and rename .env.example to .env
	
- Create a database and update database information in the .env file

- Run Composer Update

	composer update

- Genereate new app key:

	php artisan key:generate
	
- Create tables:

	php artisan migrate

## Documention

### API

**Response**
Using Jsend response format
[API Response](http://labs.omniti.com/labs/jsend)

> For testing purposes there is 3 endpoint. set these keys in each header request:
	- Content-Type	application/json
	- Accept		application/json

**Signup** *Used to signup new or current user*

    POST api/v1.0/auth/signup
    Form data:
        phone_number:	{phone number}
    Headers:
        As described in the top
   
**Signin** *Used to get api token after signup*

    POST /api/v1.0/auth/signin
    Form data:
        phone_number:			{phone number}
        verification_code:	{4 digit verification code}
    Headers:
        As described in the top
        
**Recovery** *Used to recover api token only for existing users*

    POST /api/v1.0/auth/recovery
    Form data:
        phone_number:	{phone number}
    Headers:
        As described in the top

### 4- Testing
- Run PHPUnit Test by runing this command

	./vendor/bin/phpunit

### Laravel 5.7
