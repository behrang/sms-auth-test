<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Signin extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'jsend' => 'version: 1.0',
            'status' => 'success',
            'data' => [
                'token' => $this->token
            ],
        ];
    }
}
