<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\Signup as SignupResource;
use App\Http\Requests\Signup as SignupRequest;
use App\User;
use App\Http\Requests\Signin as SigninRequest;
use App\Http\Resources\Error as ErrorResource;
use App\Models\Error;
use App\Models\Apitoken;
use App\Http\Resources\Signin as SigninResource;
use App\Http\Requests\Recovery as RecoveryRequest;

class AuthController extends ApiController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    
    public function signup(SignupRequest $request)
    {
        $phoneNumber = $request->get('phone_number');
        
        $user = User::where('phone_number', $phoneNumber)->with('Apitokens')->first();
        
        if (!$user) {
            User::create([
                'name'          => $phoneNumber,
                'email'         => $phoneNumber . '@local.com',
                'password'      => Hash::make($phoneNumber),
                'phone_number'  => $phoneNumber
                ]
            );
        }
        
        return response()->json(new SignupResource($user), 200);
    }
    
    /**
     * If verification code was correct then:
     * This endpiont makes all previous tokens as expired and create the new one
     * 
     * @param SigninRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function signin(SigninRequest $request)
    {
        $phoneNumber = $request->get('phone_number');
        $verificatioCode = $request->get('verification_code');
        
        /**
         * @todo verification code must checked here before any process
         */
        
        // Finding user by phone number
        $user = User::where('phone_number', $phoneNumber)->with('apitokens')->first();
        
        if (!$user) {
            return response()->json(new ErrorResource(new Error('User invalid')), 422);
        }
        
        // making all user api token expired and create new one
        $newApiToken = $this->createNewApiToken($user);
       
        return response()->json(new SigninResource($newApiToken), 200);
    }
    
    /**
     * Recover token if token is set
     * 
     * @param RecoveryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function recovery(RecoveryRequest $request)
    {
        $phoneNumber = $request->get('phone_number');
        
        $user = User::where('phone_number', $phoneNumber)->with('apitokens')->first();
        
        if (!$user) {
            return response()->json(new ErrorResource(new Error('User invalid')), 422);
        }
        
        if (!$user->hasActiveToken()) {
            return response()->json(new ErrorResource(new Error('The token is expired please signup again to get new one')), 422);
        }
        
        // making all user api token expired and create new one
        $newApiToken = $this->createNewApiToken($user);
        
        return response()->json(new SigninResource($newApiToken), 200);
    }
    
    protected function createNewApiToken(User $user)
    {
        // making all user api token expired
        $user->makeAllTokensExpired();
        
        // creating new api token
        $newApiToken = Apitoken::create([
            'user_id'   => $user->id,
            'token'     => bin2hex(random_bytes(25))
        ]);
        
        return $newApiToken;
    }
}
