<?php

namespace App\Models;

final class Error
{
    public $message = '';
    
    public function __construct($message = null)
    {
        if ($message) {
            $this->addMessage($message);
        }
    }
    
    public function addMessage($message)
    {
        $this->message = $message;
    }
}
