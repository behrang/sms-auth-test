<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Apitoken extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'token',
    ];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * Get the user that owns the apitoken.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    /**
     * Checking to see is token expired or not
     * @return boolean
     */
    public function isExpired()
    {
        $diff = Carbon::parse($this->created_at)->diffInDays(Carbon::now());
        if ($diff > 30) {
            return true;
        }
        return false;
    }
    
    /**
     * Setting token as expired, soft delete token
     * 
     * @return \App\Models\Apitoken
     */
    public function setExpired()
    {
        $this->delete();
        return $this;
    }
    
    
    
}
