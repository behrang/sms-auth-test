<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Apitoken;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone_number',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
     * Get the api token for the user.
     */
    public function apitokens()
    {
        return $this->hasMany('App\Models\Apitoken');
    }
    
    /**
     * Checking for active token
     * 
     * @return boolean
     */
    public function hasActiveToken()
    {
        $apiTokens = $this->apitokens;
        foreach ($apiTokens as $apiToken) {
            if (!$apiToken->isExpired()) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Returning user api token
     * 
     * @return Apitoken|NULL
     */
    public function getActiveApiToken()
    {
        $apiTokens = $this->apitokens;
        foreach ($apiTokens as $apiToken) {
            if (!$apiToken->isExpired()) {
                return $apiToken;
            }
        }
        
        return null;
    }
    
    /**
     * Making all tokens soft delete
     * 
     * @return \App\User
     */
    public function makeAllTokensExpired()
    {
        $apiTokens = $this->apitokens;
        foreach ($apiTokens as $apiToken) {
            $apiToken->setExpired();
        }
        
        return $this;
    }
    
}
